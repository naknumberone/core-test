import React from 'react';
import Core from './core/core';

function Button() {
    return (
        <div>
            <button onClick={()=>Core.dataStore.dispatch('buttonClick', {something: 'data'})}>Начать</button>
            <button onClick={()=>Core.dataStore.dispatch('buttonRemove', {anything: 'any'})}>remove</button>
        </div>
    );
}

export default Button;