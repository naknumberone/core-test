import React from 'react';
import './App.css';
import Button from "./Button";
import Core from './core/core';

/* Метод, который прослушивает события в плагине "шина данных"
*  */
App.update = (event, data) => {
    switch(event){
        case 'buttonClick':
            console.log('Я поймал событие! ', event, ' с данными ', data);
            break;
        case 'buttonRemove':
            console.log('Событие ', event, ' do something with data ', data);
            break;
        default:
            break;
    }
};

/* Обращение к плагину после загрузки плагинов */
Core.pluginsLoaded(()=>{
    Core.dataStore.subscribe(App, ['buttonClick', 'buttonRemove']);
});

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Button />
      </header>
    </div>
  );
}

export default App;
