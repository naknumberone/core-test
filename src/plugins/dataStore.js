/* Плагин шины данных */
export default class DataStore {
    constructor(){
        this.subscribers = {};
    }

    /* Подписать компонент на события */
    subscribe(sub, events){
        sub.update = sub.update || function(){};
        events.forEach(event => {
            if(this.subscribers[event]){
                this.subscribers[event].push(sub);
            } else {
                this.subscribers[event] = [ sub ];
            }
        });
    }

    /* Отписать компонент */
    unsubscribe(sub){
        this.subscribers.splice(this.subscribers.indexOf(sub), 1);
    }

    /* Оповещение компонентов о событиях */
    notifySubscribers(event, data){
        if(this.subscribers[event]){
            this.subscribers[event].forEach(sub=>sub.update(event, data));
        }
    }

    dispatch(event, data){
        this.notifySubscribers(event, data);
    }
}