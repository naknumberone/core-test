/* Список плагинов для асинхронной загрузки ядром,
* name - имя, которое будет присвоено как свойство экземпляра ядра */

export const PLUGINS = [
    {path: 'plugins/dataStore.js', name: 'dataStore'}
];