import { PLUGINS } from './plugins';

class Core {
    constructor(){
        this.loadPlugins();
    }

    /* Асинхронная загрузка плагинов из /plugins.js */
    loadPlugins(){
        PLUGINS.forEach((item) => {
            (async () => {
                let plugin = await import('../'+item.path);
                this[item.name] = new plugin.default();
            })();
        });
    }

    /* Костыль, или ???нет???
    * Решает проблему ожидания асинхронной загрузки всех плагинов из списка,
    * Что бы компоненты не обращались к свойствам undefined */
    pluginsLoaded(f){
        setTimeout(()=>f(), 1000);
    }
}

export default (new Core());